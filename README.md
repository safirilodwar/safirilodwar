# safirilodwar

Tovuti hii ina habari ya kusafiri [Lodwar](https://sw.wikipedia.org/wiki/Lodwar). Maongezo zinaombwa.

Mchezo [hapa](https://safirilodwar.gitlab.io/safirilodwar/mashindano/v4.final.html) imetengenezwa kutoka
[Jake Gordon's Javascript Racer](https://github.com/jakesgordon/javascript-racer). Maongezo zinaombwa pia.

===============

This is a website on travel to and from [Lodwar](https://en.wikipedia.org/wiki/Lodwar). Contributions are welcome.

The game [here](https://safirilodwar.gitlab.io/safirilodwar/mashindano/v4.final.html) is slightly modified from 
[Jake Gordon's Javascript Racer](https://github.com/jakesgordon/javascript-racer), contributions to this are also welcome.
